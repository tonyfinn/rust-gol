use std::fmt;

pub struct World {
    pub width: i32,
    pub height: i32,
    pub cells: Vec<bool>
}

impl World {
    pub fn tick(&self) -> World {
        let mut new_cells = vec![false; (self.width * self.height) as usize];
        for y in 0..self.height {
            for x in 0..self.width {
                new_cells[self.array_offset(x, y)] = get_updated_cell(self, x, y)
            }
        }
        World {
            width: self.width,
            height: self.height,
            cells: new_cells
        }
    }
    fn array_offset(&self, x: i32, y: i32) -> usize {
        let ypos = if y >= 0 { y % self.height } else { self.height + y };
        let xpos = if x >= 0 { x % self.width } else { self.width + x };
        let index = (ypos * self.width) + xpos;

        index as usize
    }
    pub fn get_cell(&self, x: i32, y: i32) -> bool {
        self.cells[self.array_offset(x, y)]
    }
    pub fn with_cells_enabled(&self, added_cells: &Vec<(u32, u32)>) -> World {
        let mut new_cells = vec![false; (self.width * self.height) as usize];
        for y in 0..self.height {
            for x in 0..self.width {
                new_cells[self.array_offset(x, y)] = if added_cells.contains(&(x as u32, y as u32)) {
                    true
                } else {
                    self.get_cell(x, y)
                };
            }
        }
        World {
            width: self.width,
            height: self.height,
            cells: new_cells
        }
    }
}

impl fmt::Display for World {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut display = String::new();
        for y in 0..self.height {
            for x in 0..self.width {
                let symbol = if self.get_cell(x, y) {
                    '*'
                } else {
                    '_'
                };
                display.push(symbol)
            }
            display.push('\n')
        }
        write!(f, "{}",  display)
    }
}

fn get_updated_cell(world: &World, x: i32, y: i32) -> bool {
    let old_cell = world.get_cell(x, y);
    let neighbour_count = neighbours(world, x, y);
    if old_cell && (neighbour_count == 2 || neighbour_count == 3) {
        true
    } else if !old_cell && neighbour_count == 3 {
        true
    } else {
        false
    }
}

fn neighbours(world: &World, x: i32, y: i32) -> i32 {
    let mut count = 0;
    for (nx, ny) in neighbour_positions(x, y) {
        count += world.get_cell(nx, ny) as i32;
    }
    count
}

fn neighbour_positions(x: i32, y: i32) -> Vec<(i32, i32)> {
    vec![
        (x - 1, y - 1),
        (x - 1, y),
        (x - 1, y + 1),
        (x, y - 1),
        (x, y + 1),
        (x + 1, y - 1),
        (x + 1, y),
        (x + 1, y + 1)
    ]
}
