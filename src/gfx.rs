extern crate piston_window;

use piston_window::*;

use board::World;

const CELL_COLOR: [f32; 4] = [0.0, 0.5, 0.8, 1.0];

fn get_screen_coords(x: i64, y: i64) -> [f64; 4] {
    [
        (x * 20) as f64,
        (y * 20) as f64,
        19.0,
        19.0
    ]
}

pub fn render(world: &World, c: &Context, g: &mut G2d) {
    clear([0.0; 4], g);
    for y in 0..world.height {
        for x in 0..world.width {
            if !world.get_cell(x, y) {
                continue
            }
            let screen_coords = get_screen_coords(x as i64, y as i64);
            rectangle(CELL_COLOR,
                screen_coords,
                c.transform, g);
        }
    }
}
