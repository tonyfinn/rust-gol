extern crate piston_window;

use piston_window::*;

mod board;
mod gfx;

fn main() {
    let width = 80;
    let height = 25;
    let glider_cells = vec![
        (11, 10),
        (12, 11),
        (10, 12),
        (11, 12),
        (12, 12)
    ];
    let world = board::World {
        width: width,
        height: height,
        cells: vec![false; (width * height) as usize]
    }.with_cells_enabled(&glider_cells);
    render_window(world);
}

/*fn render_ascii(world: &board::World) {
    println!("{}", world);
    let brave_new_world = world.tick();
    println!("{}", brave_new_world);
    let brave_new_world = brave_new_world.tick();
    println!("{}", brave_new_world);
    let brave_new_world = brave_new_world.tick();
    println!("{}", brave_new_world);
    let brave_new_world = brave_new_world.tick();
    println!("{}", brave_new_world);
    let brave_new_world = brave_new_world.tick();
    println!("{}", brave_new_world);
}*/

fn render_window(world: board::World) {
    let width = (world.width * 20) as u32;
    let height = (world.height * 20) as u32;
    let mut brave_new_world = world;
    let mut counter = 0;
    let window: PistonWindow =
    WindowSettings::new("Game of Life Window", [width, height])
    .exit_on_esc(true).build().unwrap();
    for e in window {
        e.draw_2d(|c, g| {
            if counter > 10 {
                counter = 0;
                brave_new_world = brave_new_world.tick();
            } else {
                counter += 1
            }
            gfx::render(&brave_new_world, &c, g)
        });
    }
}
